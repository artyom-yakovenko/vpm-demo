namespace = (name) ->
    parent = window

    for part in name.split '.' 
        parent[part] = parent[part] or {}
        parent = parent[part]

$ () ->
    app = new Application(VPM.Config)

    app.run()