class Application

    constructor: (config) ->
        @config = config

    run: () ->
        dashboard = @buildDashboard()

        dashboard.setApp @

        dashboard.load()

        dashboard.activate 'vpm-overview'

    setting: (name) ->
        value = @config

        for part in name.split '.'
            value = value?[part]

        value

    buildDashboard: () ->
        env = do (search = window.location.search) -> 
            if search.match /^\?test\b/i 
                'test' 
            else 
                'prod'

        datasources = @setting("datasources.#{env}")

        (new DashboardBuilder(datasources)).build()