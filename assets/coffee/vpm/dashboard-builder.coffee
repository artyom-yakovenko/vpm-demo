class DashboardBuilder

    constructor: (datasources) ->
        @datasources = datasources

    build: () ->
        dashboard = new Dashboard($('#vpm-dashboard'))

        menu = new Menu($('#vpm-menu'))

        menu.bind 'select', (id) -> dashboard.activate id

        dashboard.add menu

        dashboard.add @createChartsPanel()

        return dashboard

    createChartsPanel: () ->
        panel = new ChartsPanel($('#vpm-charts'))

        panel.container.find('section').each (i, container) =>
            section = @createChartsSection $(container)

            section.hide()

            panel.add section

        return panel

    createChartsSection: ($container) ->
        section = new Section($container)

        section.container.find('article').each (i, container) =>
            chart = @createChart $(container)

            section.add chart

        return section

    createChart: ($container) ->
        id = $container.attr 'id'

        loader = @createDataLoader id

        return new (@getChartClass id)($container, loader)

    createDataLoader: (id) ->
        switch id
            # Overview:
            when 'vpm-overview-categories' 
                new DataLoaders.Overview.Loader(@datasources['overview'], 'categories')
            when 'vpm-overview-shopper-demographics'
                new DataLoaders.Overview.Loader(@datasources['overview'], 'shopper_demographics')
            # Trends:
            when 'vpm-trends-shopper-trends' 
                new DataLoaders.Trends.Loader(@datasources['trends'], 'report_transaction_by_gender_or_parent')
            when 'vpm-trends-categories' 
                new DataLoaders.Trends.Loader(@datasources['trends'], 'transaction_categories')
            when 'vpm-trends-transaction-success' 
                new DataLoaders.Trends.Loader(@datasources['trends'], 'transaction_success')
            when 'vpm-trends-parent-approval' 
                new DataLoaders.Trends.Loader(@datasources['trends'], 'parent_approval')
            when 'vpm-trends-transaction-by-hour' 
                new DataLoaders.Trends.Loader(@datasources['trends'], 'hour')
            when 'vpm-trends-transaction-payment-type' 
                new DataLoaders.Trends.Loader(@datasources['trends'], 'payment_type')
            # Demographics:
            when 'vpm-demographics-payment-types-count'
                new DataLoaders.Demographics.Loader(@datasources['demographics'], 'payment_type_count')
            when 'vpm-demographics-payment-types-per-family'
                new DataLoaders.Demographics.Loader(@datasources['demographics'], 'payment_type_per_family')
            when 'vpm-demographics-children-per-family'
                new DataLoaders.Demographics.Loader(@datasources['demographics'], 'children_per_family')
            when 'vpm-demographics-children-settings-allowance-groups'
                new DataLoaders.Demographics.Loader(@datasources['demographics'], 'child_settings_allowance_groups')
            when 'vpm-demographics-transactions-per-family'
                new DataLoaders.Demographics.Loader(@datasources['demographics'], 'transaction_per_family')
            when 'vpm-demographics-transactions-by-allowance'
                new DataLoaders.Demographics.Loader(@datasources['demographics'], 'transaction_by_allowance')
            # Transactions:
            when 'vpm-transactions-details'
                new DataLoaders.Transactions.Loader(@datasources['transactions'])
            else
                throw new Error("The data loader for '#{id}' is not found")

    getChartClass: (id) ->
        switch id
            # Overview:
            when 'vpm-overview-categories' 
                Charts.Overview.Categories
            when 'vpm-overview-shopper-demographics'
                Charts.Overview.ShopperDemographics
            # Trends:
            when 'vpm-trends-shopper-trends' 
                Charts.Trends.ShopperTrends
            when 'vpm-trends-categories' 
                Charts.Trends.Categories
            when 'vpm-trends-transaction-success' 
                Charts.Trends.TransactionSuccess
            when 'vpm-trends-parent-approval' 
                Charts.Trends.ParentApproval
            when 'vpm-trends-transaction-by-hour' 
                Charts.Trends.TransactionByHour
            when 'vpm-trends-transaction-payment-type' 
                Charts.Trends.TransactionPaymentType
            # Demographics:
            when 'vpm-demographics-payment-types-count'
                Charts.Demographics.PaymentTypesCount
            when 'vpm-demographics-payment-types-per-family'
                Charts.Demographics.PaymentTypesPerFamily
            when 'vpm-demographics-children-per-family'
                Charts.Demographics.ChildrenPerFamily
            when 'vpm-demographics-children-settings-allowance-groups'
                Charts.Demographics.ChildrenSettingsAllowanceGroups
            when 'vpm-demographics-transactions-per-family'
                Charts.Demographics.TransactionsPerFamily
            when 'vpm-demographics-transactions-by-allowance'
                Charts.Demographics.TransactionsByAllowance
            when 'vpm-transactions-details'
                Charts.Transactions.Details
            else
                throw new Error("The chart class for '#{id}' is not found")