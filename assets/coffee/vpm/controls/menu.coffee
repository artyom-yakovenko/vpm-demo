class Menu extends Control

    constructor: ($container) ->
        super $container

        @container.find('a').click (event) =>
            event.preventDefault()

            id = do (href = event.target.href) ->
                # extract target id from the link href:
                href.substr(href.indexOf('#') + 1)

            @notify 'select', id

    activate: (id) ->
        $link = @container.find('a[href$="#' + id + '"]')

        if $link.length is 0
            throw new Error("Cannot find menu item for target id '#{id}'")
        else
            # de-activate all menu items:
            @container.find('li').removeClass('vpm-active')

            # make this menu item active, as well as its parents:
            $link.parentsUntil(@container, 'li').addClass('vpm-active')