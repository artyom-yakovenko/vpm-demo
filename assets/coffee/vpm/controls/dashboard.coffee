class Dashboard extends Control

    constructor: ($container) ->
        super $container

        @typeSelector = new OptionsSelector(@container.find '.vpm-options[data-name = "dashboard-data-type"]')
        @typeSelector.bind 'select', (type) => 
            # TODO: quick and dirty solution - REFACTOR!
            for panel in @controls when panel instanceof ChartsPanel
                for section in panel.controls when section instanceof Section
                    for chart in section.controls when chart instanceof Chart
                        if chart.container.is(':visible')
                            chart.load() 
                        else
                            chart.isLoaded = no

    activate: (id) ->
        @find('vpm-menu').activate id

        @find('vpm-charts').activate id

        @updateAppearance()

    load: () ->
        @container.removeClass 'vpm-loading'

    updateAppearance: () ->
        # the visible charts appearance must alternate for better visual effect;
        # we have a special .vpm-even CSS class for the charts with even positions,
        # applying it only to the currently visible charts:

        even = (position) -> position % 2 is 0

        @container.find('section > ul > li').removeClass 'vpm-even'

        @container.find('section:visible article:visible').each (i, element) ->
            $(element).parent('li').addClass 'vpm-even' if even(i + 1) # i is zero-based