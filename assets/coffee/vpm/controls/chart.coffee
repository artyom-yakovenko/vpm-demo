class Chart extends Control

    constructor: ($container, loader) ->
        super $container

        @loader = loader
        @isLoaded = no

        @d3svg = d3.select @container.find('svg').get(0)

        @status = @container.find('.vpm-status')
        @status.hide()

        @downloadLink = new DownloadLink(@container.find 'a.vpm-download')

        if @container.find('.vpm-dates-selector').length > 0
            @datesSelector = new DatesRangeSelector(@container.find '.vpm-dates-selector')

            @datesSelector.bind 'select', (range) =>
                @load()

                @downloadLink.update do () ->
                    if (range.from and range.to) 
                        { date_from: range.from, date_to: range.to }
                    else
                        {}

    show: () ->
        super()

        if not @isLoaded
            @load()
            @isLoaded = yes

    load: (fullReload = yes) ->
        @setState 'loading', 'Loading...'

        @loader.load @settings(), (status, data) =>
            @setState 'loaded'

            if status is 'success'
                @d3svg.empty() if fullReload
                @plot data
            else
                @setState 'error', 'Error loading data'
                console.log "Error loading data for the chart '#{@id()}'"
                console.log data

    plot: (data) ->
        throw new Error('Not implemented')

    settings: () ->
        settings = {}

        do () =>
            # TODO: quick and dirty - REFACTOR!
            typeSelector = new OptionsSelector($('#vpm-dashboard .vpm-options[data-name = "dashboard-data-type"]'))
            if typeSelector.value() is 'scrubbed'
                settings['scrubbed'] = 'true'

        if @datesSelector
            do (from = @datesSelector.from(), to = @datesSelector.to()) ->
                if from and to
                    settings['date_from'] = from.toISOString()
                    settings['date_to'] = to.toISOString()

        return settings

    setState: (state, status = '') ->
        @container.removeClass name for name in ['vpm-loading', 'vpm-error']

        @status.text status

        switch state
            when 'loading' 
                @container.addClass 'vpm-loading'
                @status.show()
            when 'loaded'
                @status.hide()
            when 'error'
                @container.addClass 'vpm-error'
                @status.show()
            else 
                throw new Error("The '#{state}' state is not supported")