class ChartsPanel extends Control

    sections: () ->
        (control for control in @controls when control instanceof Section)

    activate: (id) ->
        for section in @sections()
            if section.id() is id
                section.show()
            else
                if section.find(id)
                    section.activate id
                else
                    section.hide()