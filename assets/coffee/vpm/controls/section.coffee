class Section extends Control

    charts: () ->
        (control for control in @controls when control instanceof Chart)

    activate: (id) ->
        for chart in @charts()
            if chart.id() is id
                chart.show()
            else
                chart.hide()     