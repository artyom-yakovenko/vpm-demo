namespace 'Charts.Transactions'

class Charts.Transactions.Details extends Chart

    constructor: ($container, loader) ->
        super $container, loader

        @pager = new Pager(@container.find '.vpm-pager')
        @pager.bind 'select', () => @load()

    settings: () ->
        settings = super()

        settings.from = @pager.page * @pager.recordsPerPage
        settings.num = @pager.recordsPerPage

        settings

    plot: (data) ->
        $table = @container.find('table')

        $table.find('tbody').remove()

        tbody = document.createElement('tbody')

        for row in data.rows
            tr = document.createElement('tr')

            for value in row
                $(tr).append("<td>#{value}</td>")

            tbody.appendChild(tr)

        $table.append(tbody)