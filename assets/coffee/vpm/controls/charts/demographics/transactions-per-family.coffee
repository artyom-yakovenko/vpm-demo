namespace 'Charts.Demographics'

class Charts.Demographics.TransactionsPerFamily extends Chart

    constructor: ($container, loader) ->
        super $container, loader

        @typeSelector = new OptionsSelector(@container.find '.vpm-options')
        @typeSelector.bind 'select', (view) => @load()

    settings: () ->
        settings = super()

        settings.type = @typeSelector.value()

        settings

    plot: (data) ->
        chart = nv.models.multiBarHorizontalChart()
                         .x((d) -> d.label)
                         .y((d) -> d.value)
                         .margin(@app.setting 'margin')
                         .stacked(true)
                         .tooltips(true)
                         .showControls(false)
                         .showLegend(false)
                         .showValues(false)
                         .transitionDuration(500)

        chart.yAxis.tickFormat d3.format(',.0f')

        values = [
            key: 'Transactions per Family'
            color: @app.setting 'colors.default'
            values: data.values
        ]

        @d3svg.datum(values).call(chart)