namespace 'Charts.Demographics'

class Charts.Demographics.PaymentTypesCount extends Chart

    constructor: ($container, loader) ->
        super $container, loader

        @typeSelector = new OptionsSelector(@container.find '.vpm-options')
        @typeSelector.bind 'select', (view) => @load()

    settings: () ->
        settings = super()

        settings.transacting = @typeSelector.value() is 'transacting'

        settings

    plot: (data) ->
        chart = nv.models.multiBarHorizontalChart()
                         .x((d) -> d.label)
                         .y((d) -> d.value)
                         .margin(@app.setting 'margin')
                         .stacked(true)
                         .tooltips(true)
                         .showControls(false)
                         .showLegend(false)
                         .showValues(false)
                         .transitionDuration(500)

        chart.yAxis.tickFormat d3.format(',.0f')

        values = [
            key: 'Payment Types'
            color: @app.setting 'colors.default'
            values: data.values
        ]

        @d3svg.datum(values).call(chart)