class DatesRangeSelector extends Control

    constructor: ($container) ->
        super $container

        @container.find('input:text').each (i, field) =>
            $(field).datepicker
                dateFormat: 'M d yy'
                maxDate: new Date()
                onSelect: () =>
                    @notify 'select', { from: @from(), to: @to() }

    from: () ->
        @date('from')

    to: () ->
        @date('to')

    date: (name) ->
        @container.find("input[name = '#{name}']").datepicker('getDate')