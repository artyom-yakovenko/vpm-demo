namespace 'Charts.Overview'

class Charts.Overview.Categories extends Chart

    constructor: ($container, loader) ->
        super $container, loader

        @typeSelector = new OptionsSelector(@container.find '.vpm-options')
        @typeSelector.bind 'select', (view) => @load()

    settings: () ->
        settings = super()

        settings.type = @typeSelector.value()

        settings

    plot: (data) ->
        chart = nv.models.multiBarHorizontalChart()
                         .x((d) -> d.label)
                         .y((d) -> d.value)
                         .margin(@app.setting 'margin')
                         .stacked(true)
                         .tooltips(true)
                         .showControls(false)
                         .showLegend(true)
                         .showValues(false)
                         .transitionDuration(500)

        chart.yAxis.tickFormat (d) -> '$' + (d3.format(',.0f') d)
        
        girls =
            key: 'Girls'
            color: @color('Girls')
            values: data.girls

        boys =
            key: 'Boys'
            color: @color('Boys')
            values : data.boys

        parents =
            key: 'Parents'
            color: @color('Parents')
            values : data.parents

        @d3svg.datum([girls, boys, parents]).call(chart)

    color: (group) ->
        @app.setting('colors.gender')[group]