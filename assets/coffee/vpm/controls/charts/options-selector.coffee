class OptionsSelector extends Control

    constructor: ($container) ->
        super $container

        $container.find('input:radio').click (e) =>
            @notify 'select', $(e.target).val()

    value: () ->
        @container.find('input:radio:checked').val()