class DownloadLink extends Control

    constructor: ($container) ->
        super $container

        @url = @container.attr('href')

    update: (context) ->
        params = ("#{name}=#{encodeURIComponent context[name]}" for name of context when context[name])

        if params.length is 0
            @container.attr('href', @url)
        else
            glue = if @url.match(/\?/) then '&' else '?'

            @container.attr('href', @url + glue + params.join('&'))