class AgeGroupsSelector extends Control

    constructor: ($container) ->
        super $container

        @container.find('a').click (e) =>
            e.preventDefault()

            $(e.target).parent('li').toggleClass 'vpm-active'

            @notify 'select', $(e.target).attr('data-group')

    groups: () ->
        groups = []

        @container.find('li.vpm-active').each (i, li) ->
            groups.push $(li).find('a').attr('data-group')

        return groups

    getGroupColor: (group) ->
        return @container.find("a[data-group = '#{group}']").css('background-color')