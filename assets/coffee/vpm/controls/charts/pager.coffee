class Pager extends Control

    constructor: ($container) ->
        super $container

        @page = 0
        @recordsPerPage = 10

        $container.find('a').click (e) =>
            e.preventDefault()

            if $(e.target).not('.disabled')
                type = $(e.target).attr('data-type')

                if type is 'next'
                    @page += 1
                else
                    @page -= 1 if @page > 0

                do ($prev = @container.find('a[data-type = "prev"]')) =>
                    if @page is 0 
                        $prev.addClass('disabled') if not $prev.hasClass('disabled')
                    else 
                        $prev.removeClass('disabled')

                @notify 'select', @page