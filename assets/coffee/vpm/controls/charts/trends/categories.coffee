namespace 'Charts.Trends'

class Charts.Trends.Categories extends Chart

    constructor: ($container, loader) ->
        super $container, loader

        @typeSelector = new OptionsSelector(@container.find '.vpm-options')
        @typeSelector.bind 'select', (view) => @load()

    settings: () ->
        settings = super()

        settings.type = @typeSelector.value()

        settings

    plot: (data) ->
        chart = nv.models.multiBarChart()
                         .x((d) -> d.label)
                         .y((d) -> d.value)
                         .margin(@app.setting 'margin')
                         .stacked(true)
                         .tooltips(true)
                         .showControls(false)
                         .showLegend(true)
                         .reduceXTicks(false)
                         .transitionDuration(500)

        chart.yAxis.tickFormat d3.format(',.0f')

        beauty_accessories =
            key: 'Beauty & Accessories'
            values: data.beauty_accessories

        books_media =
            key: 'Books & Media'
            color: @color 'Gift Cards'
            values: data.books_media

        clothing_shoes =
            key: 'Clothing & Shoes'
            color: @color 'Clothing & Shoes'
            values : data.clothing_shoes

        electronics =
            key: 'Electronics'
            color: @color 'Gaming'
            values: data.electronics

        gift_cards =
            key: 'Gift Cards'
            color: @color 'Gift Cards'
            values: data.gift_cards

        home_decor =
            key: 'Home Decor'
            color: @color 'Home Decor'
            values: data.home_decor

        miscellaneous =
            key: 'Misc'
            color: @color 'Misc'
            values: data.miscellaneous

        online_gaming =
            key: 'Online Gaming'
            color: @color 'Online Gaming'
            values: data.online_gaming

        sporting_goods =
            key: 'Sporting Goods'
            color: @color 'Sporting Goods'
            values: data.sporting_goods

        toys =
            key: 'Toys'
            color: @color 'Toys'
            values: data.toys

        @d3svg.datum([beauty_accessories, books_media, clothing_shoes, electronics, gift_cards, home_decor, miscellaneous, online_gaming, sporting_goods, toys]).call(chart)

    color: (category) ->
        @app.setting('colors.categories')[category]