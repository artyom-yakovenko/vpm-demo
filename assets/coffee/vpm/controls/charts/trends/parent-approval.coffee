namespace 'Charts.Trends'

class Charts.Trends.ParentApproval extends Chart

    constructor: ($container, loader) ->
        super $container, loader

        @typeSelector = new OptionsSelector(@container.find '.vpm-options')
        @typeSelector.bind 'select', (view) => @load()

    settings: () ->
        settings = super()

        settings.type = @typeSelector.value()

        settings

    plot: (data) ->
        chart = nv.models.multiBarChart()
                         .x((d) -> d.label)
                         .y((d) -> d.value)
                         .margin(@app.setting 'margin')
                         .stacked(true)
                         .tooltips(true)
                         .showControls(false)
                         .showLegend(true)
                         .reduceXTicks(false)
                         .transitionDuration(500)

        chart.yAxis.tickFormat d3.format(',.0f')

        approved =
            key: 'Approved'
            color: @color 'Approved'
            values: data.approved

        rejected =
            key: 'Rejected'
            color: @color 'Payment Rejected'
            values : data.rejected

        pending =
            key: 'Pending'
            color: @color 'Pending'
            values : data.pending

        @d3svg.datum([approved, rejected, pending]).call(chart)

    color: (status) ->
        @app.setting('colors.status')[status]