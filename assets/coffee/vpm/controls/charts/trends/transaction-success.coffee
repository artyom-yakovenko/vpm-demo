namespace 'Charts.Trends'

class Charts.Trends.TransactionSuccess extends Chart

    constructor: ($container, loader) ->
        super $container, loader

        @typeSelector = new OptionsSelector(@container.find '.vpm-options')
        @typeSelector.bind 'select', (view) => @load()

    settings: () ->
        settings = super()

        settings.type = @typeSelector.value()

        settings

    plot: (data) ->
        chart = nv.models.multiBarChart()
                         .x((d) -> d.label)
                         .y((d) -> d.value)
                         .margin(@app.setting 'margin')
                         .stacked(true)
                         .tooltips(true)
                         .showControls(false)
                         .showLegend(true)
                         .reduceXTicks(false)
                         .transitionDuration(500)

        chart.yAxis.tickFormat d3.format(',.0f')

        processed =
            key: 'Successful'
            color: @color 'Successful'
            values: data.processed

        mismatch =
            key: 'Payment Mismatch'
            color: @color 'Payment Mismatch'
            values : data.mismatch

        rejected =
            key: 'Rejected'
            color: @color 'Rejected'
            values : data.rejected

        pending =
            key: 'Pending'
            color: @color 'Pending'
            values : data.pending

        cancelled =
            key: 'Merchant Cancelled'
            color: @color 'Merchant Cancelled'
            values : data.cancelled

        @d3svg.datum([processed, mismatch, rejected, pending, cancelled]).call(chart)

    color: (status) ->
        @app.setting('colors.status')[status]