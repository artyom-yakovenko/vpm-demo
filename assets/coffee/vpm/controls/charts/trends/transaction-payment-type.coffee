namespace 'Charts.Trends'

class Charts.Trends.TransactionPaymentType extends Chart

    constructor: ($container, loader) ->
        super $container, loader

        @typeSelector = new OptionsSelector(@container.find '.vpm-options')
        @typeSelector.bind 'select', (view) => @load()

    settings: () ->
        settings = super()

        settings.type = @typeSelector.value()

        settings

    plot: (data) ->
        chart = nv.models.multiBarChart()
                         .x((d) -> d.label)
                         .y((d) -> d.value)
                         .margin(@app.setting 'margin')
                         .stacked(true)
                         .tooltips(true)
                         .showControls(false)
                         .showLegend(true)
                         .reduceXTicks(false)
                         .transitionDuration(500)

        chart.yAxis.tickFormat d3.format(',.0f')

        cc =
            key: 'Credit Card'
            values: data.credit_card

        paypal =
            key: 'PayPal'
            values : data.paypal

        @d3svg.datum([cc, paypal]).call(chart)