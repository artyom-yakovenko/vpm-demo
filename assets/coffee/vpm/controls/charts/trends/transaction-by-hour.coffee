namespace 'Charts.Trends'

class Charts.Trends.TransactionByHour extends Chart

    constructor: ($container, loader) ->
        super $container, loader

        @typeSelector = new OptionsSelector(@container.find '.vpm-options')
        @typeSelector.bind 'select', (view) => 
            @load()

        @timeSelector = new OptionsSelector(@container.find '.vpm-options[data-name = "time"]')
        @timeSelector.bind 'select', (view) => 
            @updateTimeAxisLabel()
            @load()

    settings: () ->
        settings = super()

        settings.type = @typeSelector.value()

        if @timeSelector.value() is 'local'
            settings.time_zone = @resolveLocalTimezoneParameter()

        settings

    plot: (data) ->
        chart = nv.models.multiBarChart()
                         .x((d) -> d.label)
                         .y((d) -> d.value)
                         .margin(@app.setting 'margin')
                         .stacked(true)
                         .tooltips(true)
                         .showControls(false)
                         .showLegend(true)
                         .reduceXTicks(false)
                         .transitionDuration(500)

        chart.yAxis.tickFormat d3.format(',.0f')

        boys =
            key: 'Boys'
            color: @color 'Boys'
            values: data.boys

        girls =
            key: 'Girls'
            color: @color 'Girls'
            values : data.girls

        parents =
            key: 'Parents'
            color: @color 'Parents'
            values: data.parents

        @d3svg.datum([boys, girls, parents]).call(chart)

    color: (group) ->
        @app.setting('colors.gender')[group]

    updateTimeAxisLabel: () ->
        time = do () => 
            if @timeSelector.value() is 'local'
                'Users Local Time'
            else
                'GMT'

        @container.find('.vpm-axis-x').text("Time (#{time})")

    resolveLocalTimezoneParameter: () ->
        offset = (new Date()).getTimezoneOffset()

        sign = if offset > 0 then '+' else '-'

        hours = Math.floor(Math.abs(offset) / 60)

        minutes = Math.abs(offset) - hours * 60
        minutes = "0#{minutes}" if minutes < 10

        "#{sign}#{hours}:#{minutes}"