namespace 'DataLoaders.Demographics'

class DataLoaders.Demographics.Loader extends DataLoader

    constructor: (url, key) ->
        super url

        @key = key

    processResponse: (params, response) ->
        do (data = response[@key]) =>
            switch @key
                when 'payment_type_count' 
                    @processPaymentTypesCount data
                when 'payment_type_per_family' 
                    @processPaymentTypesPerFamily data
                when 'children_per_family' 
                    @processChildrenPerFamily data
                when 'child_settings_allowance_groups' 
                    @processChildrenSettingsAllowanceGroups data
                when 'transaction_per_family' 
                    @processTransactionsPerFamily data
                when 'transaction_by_allowance' 
                    @processTransactionsByAllowance data
                else 
                    throw new Error("Demographics data loader not found for '#{@key}'")

    processPaymentTypesCount: (data) ->
        labels = for card in data.cards
            switch card
                when 'AmericanExpr' then 'American Express'
                when 'Paypal' then 'PayPal'
                else card

        data = 
            values: @zip labels, data.num

        return data

    processPaymentTypesPerFamily: (data) ->
        data.label.reverse()
        data.count.reverse()

        data = 
            values: @zip data.label, data.count

        return data

    processChildrenPerFamily: (data) ->
        data.children.reverse()
        data.num.reverse()

        data = 
            values: @zip data.children, data.num

        return data

    processChildrenSettingsAllowanceGroups: (data) ->
        data[key].reverse() for own key of data

        data = 
            boys: @zip data.labels, data.boys
            girls: @zip data.labels, data.girls

        return data

    processTransactionsPerFamily: (data) ->
        data.labels.reverse()
        data.values.reverse()

        data = 
            values: @zip data.labels, data.values

        return data

    processTransactionsByAllowance: (data) ->
        data = 
            values: @zip data.labels, data.values

        return data