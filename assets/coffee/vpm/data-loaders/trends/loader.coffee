namespace 'DataLoaders.Trends'

class DataLoaders.Trends.Loader extends DataLoader

    constructor: (url, key) ->
        super url

        @key = key

    processResponse: (params, response) ->
        do (data = response[@key]) =>
            switch @key
                when 'transaction_categories' 
                    @processCategories data
                when 'report_transaction_by_gender_or_parent' 
                    @processShopperTrends data
                when 'transaction_success' 
                    @processTransactionSuccess data
                when 'parent_approval' 
                    @processParentApproval data
                when 'hour' 
                    @processTransactionByHour data
                when 'payment_type' 
                    @processTransactionPaymentType data
                else 
                    throw new Error("Trends data loader not found for '#{@key}'")

    processCategories: (data) ->
        data =
            beauty_accessories: @zip data.labels, data.values.beauty_accessories
            books_media: @zip data.labels, data.values.books_media
            clothing_shoes: @zip data.labels, data.values.clothing_shoes
            electronics: @zip data.labels, data.values.electronics
            gift_cards: @zip data.labels, data.values.gift_cards
            home_decor: @zip data.labels, data.values.home_decor
            miscellaneous: @zip data.labels, data.values.miscellaneous
            online_gaming: @zip data.labels, data.values.online_gaming
            sporting_goods: @zip data.labels, data.values.sporting_goods
            toys: @zip data.labels, data.values.toys

        return data

    processShopperTrends: (data) ->
        data =
            boys: @zip data.labels, data.values.boys
            girls: @zip data.labels, data.values.girls
            parents: @zip data.labels, data.values.parents

        return data

    processTransactionSuccess: (data) ->
        data = 
            processed: @zip data.labels, data.values.processed
            mismatch: @zip data.labels, data.values.mismatch
            rejected: @zip data.labels, data.values.rejected
            pending: @zip data.labels, data.values.pending
            cancelled: @zip data.labels, data.values.cancelled

        return data

    processParentApproval: (data) ->
        data = 
            approved: @zip data.labels, data.values.approved
            rejected: @zip data.labels, data.values.rejected
            pending: @zip data.labels, data.values.pending

        return data

    processTransactionByHour: (data) ->
        values = data.groups.boys.concat(data.groups.girls).concat(data.groups.parents)

        data =
            range: [0, @max values]
            boys: @zip data.labels, data.groups.boys
            girls: @zip data.labels, data.groups.girls
            parents: @zip data.labels, data.groups.parents

        return data

    processTransactionPaymentType: (data) ->
        data = 
            credit_card: @zip data.labels, data.values.credit_card
            paypal: @zip data.labels, data.values.paypal

        return data

    max: (values) ->
        max = Math.max.apply(null, values)

        magnitude = Math.pow(10, Math.floor(Math.log(max) / Math.LN10))

        max = Math.ceil(max / magnitude) * magnitude

        return max