namespace 'DataLoaders.Overview'

class DataLoaders.Overview.Loader extends DataLoader

    constructor: (url, key) ->
        super url

        @key = key

    processResponse: (params, response) ->
        do (data = response[@key]) =>
            switch @key
                when 'categories' 
                    @processCategories data
                when 'shopper_demographics'
                    @processShopperDemographics data
                else 
                    throw new Error("Overview data loader not found for '#{@key}'")

    processCategories: (data) ->
        data =
            boys: @zip data.labels, data.groups.boys
            girls: @zip data.labels, data.groups.girls
            parents: @zip data.labels, data.groups.parents

        return data

    processShopperDemographics: (data) ->
        labels = ("#{label} yrs" for label in data.labels)
        
        data =
            boys: @zip labels, data.values.boys
            girls: @zip labels, data.values.girls
            parents: @zip labels, data.values.parents

        return data