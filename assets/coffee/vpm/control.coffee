class Control

    constructor: ($container) ->
        @app = null
        @container = $container
        @controls = []
        @callbacks = {}

    id: () ->
        @container.attr('id')

    add: (control) ->
        @controls.push control

    show: () ->
        @container.show()

        control.show() for control in @controls

    hide: () ->
        @container.hide()

        control.hide() for control in @controls

    find: (id) ->
        for control in @controls
            if control.id() is id
                return control

        return null

    bind: (event, callback) ->
        if event of @callbacks
            @callbacks[event].push callback
        else
            @callbacks[event] = [callback]

    notify: (event, context) ->
        if event of @callbacks
            callback(context) for callback in @callbacks[event]

    setApp: (app) ->
        @app = app

        control.setApp app for control in @controls