class DataLoader

    constructor: (url) ->
        @url = url

    load: (params, callback) ->
        $.ajax
            url: @url
            data: params
            type: 'GET'
            dataType: 'json'
            error: (jqXHR, textStatus, errorThrown) =>
                callback 'error', { jqXHR, textStatus, errorThrown }
            success: (response) =>
                callback 'success', @processResponse(params, response)

    processResponse: (params, response) ->
        return response

    zip: (labels, values) ->
        result = []

        for i in [0...labels.length]
            result.push 
                label: labels[i]
                value: if i < values.length then Number(values[i]) else 0

        return result