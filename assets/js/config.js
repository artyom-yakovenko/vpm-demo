var VPM = {

    Config: {

        'datasources': {
            'prod': {
                'overview': 'assets/datasets/overview.json',
                'trends': 'assets/datasets/trends.json',
                'demographics': 'assets/datasets/demographics.json',
                'transactions': 'assets/datasets/transactions.json'
            },

            'test': {
                'overview': 'assets/datasets/overview.json',
                'trends': 'assets/datasets/trends.json',
                'demographics': 'assets/datasets/demographics.json',
                'transactions': 'assets/datasets/transactions.json'
            }
        },

        'margin': { 
            'top': 30, 
            'right': 30, 
            'bottom': 50, 
            'left': 160 
        },

        'colors': {
            'default': '#1F77B4',

            'categories': {
                'Beauty & Accessories': '#F52D7B',
                'Clothing & Shoes': '#984ACA',
                'Electronics': '#D202CB',
                'Gift Cards': '#F1C40F',
                'Home & Decor': '#B7B500',
                'Online Gaming': '#F52D7B',
                'Toys': '#ECA204',
                'Books & Media': '#8F81FF',
                'Sporting Goods': '#2E65F2',
                'Charity': '#FE83E5',
                'Misc': '#66B129',
                'Restaurants': '#0DCA97',
                'Uncategorized': '#C6A2D6'
            },

            'gender': {
                'Girls': '#0DCA97',
                'Boys': '#1F77B4',
                'Parents': '#F2AC00',
                'Totals': '#F2AC00'
            },

            'status': {
                'Successful': '#1F77B4',
                'Payment Mismatch': '#999',
                'Rejected': '#ECA204',
                'Pending': '#4AD1F3',
                'Merchant Cancelled': '#333',
                'Approved': '#1F77B4',
                'Refused by Parent': '#999'
            }
        }

    }

}