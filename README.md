# VPM Demo Technical Info

## Markup
 
The VPM Demo page markup is created using HTML 5.
 
## Styles
 
Style rules are written in LESS (stored in the less folder), which is then compiled to CSS - the resulting *assets/css/main.css* is then linked to in the page.
 
More info about LESS:

* [http://en.wikipedia.org/wiki/LESS](http://en.wikipedia.org/wiki/LESS)
* [http://www.lesscss.org](http://www.lesscss.org)
* [http://winless.org](http://winless.org) - GUI Less compiler for Windows (can watch the LESS source and re-compile it to CSS in the background)
 
## Scripts
 
Client-side code is written in CoffeeScript (stored in the assets/coffee folder), which is then compiled to JavaScript - the resulting *assets/js/main.js* is then included in the page.
 
More info about CoffeeScript:

* [http://en.wikipedia.org/wiki/Coffeescript](http://en.wikipedia.org/wiki/Coffeescript)
* [http://coffeescript.org](http://coffeescript.org)

In order to install CoffeeScript on Windows:

1. Install Node.js from [http://nodejs.org](http://nodejs.org)
2. Install CoffeeScript with the node package manager from command line: `npm install -g coffee-script`
3. Write a script in your favourite text editor. Save it, say, as *hello.coffee*
4. Run your script: coffee hello.coffee or compile it to hello.js: `coffee -c hello.coffee`

In order to compile to project CoffeeScript source code to JavaScript, run the following command from within assets folder:
 
```
coffee -j js/main.js -c coffee/
```
 
While updating and testing the code, it is very convenient to add the -w option, in which case the CoffeeScript compiler will keep running and "watching" for source code changes, re-compiling it on the fly:
 
```
coffee -j js/main.js -cw coffee/
```
 
Other libraries used:

* jQuery for working with DOM: [http://jquery.com](http://jquery.com)
* D3.js for building charts: [http://d3js.org](http://d3js.org) - wonderful library!
* NVD3.js: [http://nvd3.org](http://nvd3.org) - a very helpful collection of charts built using D3.js